package tk.amplifiable.backend.extensions.custombaseexample;

import org.jetbrains.annotations.NotNull;
import tk.amplifiable.backend.extensions.ExtensionManager;
import tk.amplifiable.backend.extensions.JavaExtension;
import tk.amplifiable.backend.extensions.base.services.AdminService;

public class CustomBaseExample extends JavaExtension {
    @Override
    public void load(@NotNull ExtensionManager extensionManager) {
        extensionManager.removeExtension("base");
        getLogger().info("Removed default base extension");
        registerService(new AdminService());
    }
}
